Jython Jar Maker
================

Use jar-maker.py -h for help in usage.

Run commands from there to build a .jar file for distribution.

Once installed with the setup.py file, use jython -m jar-maker [commands].

entrypoint.py
-------------

An entrypoint.py file will need to be created. It should import the main function of your script. It should be placed in the root of your script's directory.

ex: from myscript import main


Commands
--------

* (-i, --initial) Use this for the intial creation of the Package directory
* (-m, --make-jar) Create the .jar file with specified name
* (-a, --add-modules) Add modules to an existing Package directory (overwrites)
* (-j, --jython-home) Specify the Jython root directory. Used with -i when creating the initial Package directory.
     
