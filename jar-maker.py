#$ mkdir Package
#$ cd Package
#$ cp -r JYTHONROOT/Lib .
#$ unzip JYTHONROOT/jython.jar
#$ # add your modules to Lib
#$ javac ../Main.java -d .
#$ cp ../entrypoint.py .
#$ jar -cfe output.jar Main *
"""
Used to create a standalone .jar file with Jython.

"""


import os
import shutil
import sys
import subprocess
import optparse
parser = optparse.OptionParser()
parser.add_option("-i", "--initial", action = 'store_true', dest = "initial" , default = False,
                  help = "This is for the initial creation of the Package directory")
parser.add_option("-j", "--jython-home", type = "string", action = "store", dest = "jython_root",
                  help = "Path to Jython root")
parser.add_option("-a", "--add-modules", type = "string", action = "store", dest = "modules",
                  default = None, help = "Add modules to the Lib")
parser.add_option("-m", "--make-jar", type = "string", action = "store", dest = "jar_name",
                   help = "Create a .jar file with specified name")
(options, args) = parser.parse_args()

java_stuff = """
import java.io.FileInputStream;
import java.lang.System;
import java.util.Properties;

import org.python.core.Py;
import org.python.core.PyException;
import org.python.core.PyFile;
import org.python.core.PySystemState;
import org.python.util.JLineConsole;
import org.python.util.InteractiveConsole;
import org.python.util.InteractiveInterpreter;

public class Main {
    private static InteractiveConsole newInterpreter(boolean interactiveStdin) {
        if (!interactiveStdin) {
            return new InteractiveConsole();
        }

        String interpClass = PySystemState.registry.getProperty(
        "python.console", "");
        if (interpClass.length() > 0) {
            try {
                return (InteractiveConsole)Class.forName(
            interpClass).newInstance();
            } catch (Throwable t) {
                // fall through
            }
        }
        return new JLineConsole();
    }

    public static void main(String[] args) throws PyException {
        PySystemState.initialize(
            PySystemState.getBaseProperties(), 
            new Properties(), args);

        PySystemState systemState = Py.getSystemState();
        // Decide if stdin is interactive
        boolean interactive = ((PyFile)Py.defaultSystemState.stdin).isatty();
        if (!interactive) {
            systemState.ps1 = systemState.ps2 = Py.EmptyString;
        }

        // Now create an interpreter
        InteractiveConsole interp = newInterpreter(interactive);
        systemState.__setattr__("_jy_interpreter", Py.java2py(interp));
        interp.exec("try:\\n import entrypoint\\n entrypoint.main()\\nexcept SystemExit: pass");
    }
}
"""


command = sys.argv[1]
#jython_root = sys.argv[2]
script_dir = os.path.dirname(os.path.realpath(__file__))
current_directory = os.getcwd()

def copy_main_java():
    print "....Copying Main.java"
    #shutil.copy(os.path.join(script_dir, 'Main.java'), current_directory)
    java_file = open('Main.java', 'w')
    java_file.write(java_stuff)
    java_file.close()
    print "....Copied Main.java"
    
def copy_jython():
    print "....Copying jython/Lib"
    shutil.copytree(os.path.join(options.jython_root, "Lib"), \
            os.path.join(current_directory, "Package/Lib/"))
    print "....Copied jython/Lib"

def unzip_jython():
    print "....Unzipping jython-jar"
    os.chdir(os.path.join(os.getcwd(), "Package"))
    subprocess.call("unzip " + os.path.join(options.jython_root, "jython.jar"),\
            shell = True)
    os.chdir(current_directory)
    print "....Unzipped jython-jar"

def add_modules(modules):
    print "....Adding your modules"
    for module in modules.split(','):
        shutil.copy(os.path.join(current_directory, module), \
                os.path.join(current_directory, "Package/Lib"))
    print "....Added modules to Lib"

def make_entry():
    pass

def run_javac():
    print "....Running javac"
    os.chdir(os.path.join(current_directory, "Package"))
    subprocess.call("javac ../Main.java -d .", shell = True)
    print "....Ran javac"

def copy_entry():
    print "....Copying entrypoint.py"
    subprocess.call("cp ../entrypoint.py .", shell = True)
    print "....Copied entrypoint.py"

def make_jar():
    print "....Making .jar file"
    command = "jar -cfe %s Main *" % (options.jar_name)
    subprocess.call(command, shell = True) 
    print "....Jar created"

if options.initial:
    copy_main_java()
    copy_jython()
    unzip_jython()
    if options.modules:
        add_modules(options.modules)
    print(os.getcwd())
    run_javac()
    copy_entry()
    make_jar()
else:
    if options.modules:
        add_modules(options.modules)
#elif options.command == 'make_entry':
#    make_entry()
    
